import * as crypto from '@shardus/crypto-utils';
import {readdir, stat} from 'fs';
import {readFile} from 'fs/promises';
import {performance} from 'perf_hooks';
const path = require('path');
crypto.init(crypto.randomBytes());

export const generateRandomHash = () => {
  const obj = {
    hash: crypto.randomBytes(),
    timstamp: new Date().toString(),
  };
  return crypto.hashObj(obj);
};

export const exitHandler = (cb: Function) => {
  process.on('SIGINT', () => {
    //graceful shutdown
    cb();
  });
  process.on('SIGQUIT', () => {
    //graceful shutdown
    cb();
  });
  process.on('SIGTERM', () => {
    //graceful shutdown
    cb();
  });
};

export const delay = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export const loadDummyData = async (path?: string) => {
  const tmp = path ? path : './sample.json';
  return JSON.parse(await readFile(tmp, 'utf8'));
};

export const prepareRocksdbQuery = (count: number, data: any) => {
  const query: any[] = [];

  for (let x = 0; x < count; x++) {
    const id = generateRandomHash();
    query.push({
      type: 'put',
      key: id,
      value: data,
    });
  }

  return query;
};

export const prepareSqliteQuery = (count: number, data: any) => {
  const query: any[] = [];

  for (let x = 0; x < count; x++) {
    const id = generateRandomHash();
    query.push({
      key: id,
      value: data,
    });
  }

  return query;
};
