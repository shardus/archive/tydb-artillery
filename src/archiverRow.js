const fs = require('fs');

const generateHash = function (num) {
  const table = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];
  let hash = '';
  for (let i = 0; i < num; i++) {
    const randomIndex = Math.floor(Math.random() * table.length);
    hash += table[randomIndex];
  }
  return hash;
};

const baseRecord = {
  cycleRecord: {
    activated: [],
    activatedPublicKeys: [],
    active: 9,
    apoptosized: [],
    counter: 48,
    desired: 10,
    duration: 60,
    expired: 9,
    joined: [],
    joinedArchivers: [],
    joinedConsensors: [],
    leavingArchivers: [],
    lost: [],
    marker: 'c690e9a51428502398dfde2a660ce07bdd259551dbcd0462ec626399b436b4f1',
    networkDataHash: [
      {
        cycle: 46,
        hash: 'a516257dcd8fa314f3ed28bb1d7decf3c0411fbb261059efe2414122da124dbc',
      },
    ],
    networkId: 'e83641c2242f5d8d14f3c1ebcced1c1500fac8aad8603f8aed155d94d0987fea',
    networkReceiptHash: [
      {
        cycle: 46,
        hash: 'f550a7604b927f7dc9ff06e03f7a013478fbc2ec7fdc24689677f358459b0be2',
      },
    ],
    networkStateHash: '09f26cda190a513ba09d30ad07b6dc953b134a915f9abc827c0c7be3328d6afd',
    networkSummaryHash: [
      {
        cycle: 46,
        hash: '78c7d4bfca718a92b57a31832c1c8460f43dee960b5f4cf4bbdae3bcce2deb6d',
      },
    ],
    previous: '00039f43f6c2cb50f09998d0db4a1916908da068ed5baabcfe3fd0140441ae55',
    refreshedArchivers: [],
    refreshedConsensors: [],
    refuted: [],
    removed: [],
    returned: [],
    safetyMode: false,
    safetyNum: 0,
    start: 1651139612,
    syncing: 0,
  },
  cycleMarker: 'c690e9a51428502398dfde2a660ce07bdd259551dbcd0462ec626399b436b4f1',
  data: {
    parentCycle: 'c690e9a51428502398dfde2a660ce07bdd259551dbcd0462ec626399b436b4f1',
    networkHash: '2d785ec5801a59ffe89ec9e62d092f03d819140d6304d4fa675f5ecbe72e57cc',
    partitionHashes: {},
  },
  receipt: {
    parentCycle: 'c690e9a51428502398dfde2a660ce07bdd259551dbcd0462ec626399b436b4f1',
    networkHash: 'ebb55f25e4be33cb0c1c3c6f7793efc1060dbb8f6440ad4414ec448641583997',
    partitionHashes: {},
    partitionTxsMap: {},
  },
  summary: {
    parentCycle: 'c690e9a51428502398dfde2a660ce07bdd259551dbcd0462ec626399b436b4f1',
    networkHash: '78c7d4bfca718a92b57a31832c1c8460f43dee960b5f4cf4bbdae3bcce2deb6d',
    partitionHashes: {},
    partitionBlobs: {},
  },
};

for (let i = 0; i < 31; i++) {
  baseRecord.cycleRecord.refreshedConsensors.push({
    activeTimestamp: Date.now(),
    address: generateHash(64),
    counterRefreshed: 45,
    curvePublicKey: generateHash(64),
    cycleJoined: '0000000000000000000000000000000000000000000000000000000000000000',
    externalIp: '127.0.0.1',
    externalPort: 9005,
    id: generateHash(64),
    internalIp: '127.0.0.1',
    internalPort: 10005,
    joinRequestTimestamp: 1651136794,
    publicKey: generateHash(64),
    status: 'active',
  });
}
for (let i = 0; i < 20; i++) {
  baseRecord.cycleRecord.joinedConsensors.push({
    activeTimestamp: Date.now(),
    address: generateHash(64),
    counterRefreshed: 45,
    curvePublicKey: generateHash(64),
    cycleJoined: '0000000000000000000000000000000000000000000000000000000000000000',
    externalIp: '127.0.0.1',
    externalPort: 9005,
    id: generateHash(64),
    internalIp: '127.0.0.1',
    internalPort: 10005,
    joinRequestTimestamp: 1651136794,
    publicKey: generateHash(64),
    status: 'active',
  });
}
for (let i = 0; i < 10; i++) {
  baseRecord.cycleRecord.refreshedArchivers.push({
    curvePk: generateHash(64),
    ip: 'localhost',
    port: 4000,
    publicKey: generateHash(64),
  });
}
for (let i = 0; i < 10; i++) {
  baseRecord.cycleRecord.activated.push(generateHash(64));
  baseRecord.cycleRecord.removed.push(generateHash(64));
}

function getWrappedEthAccount(number) {
  const result = [];
  for (let i = 0; i < number; i++) {
    result.push({
      accountId: '0x2231b1d718b4fcf82404feba2268230cb5600f2bcaaf48e3da930271fe00d28a',
      data: {
        timestamp: 1651139615197,
        ethAddress: '0x2231b1d718b4fcf82404feba2268230cb5600f2bcaaf48e3da930271fe00d28a',
        accountType: 7,
        readableReceipt: {
          transactionHash: '0x2231b1d718b4fcf82404feba2268230cb5600f2bcaaf48e3da930271fe00d28a',
          transactionIndex: '0x1',
          blockNumber: '0xb',
          nonce: '0x',
          blockHash: '0xc6ef2fc5426d6ad6fd9e2a26abeab0aa2411b7ab17f30a99d3cb96aed1d1055b',
          cumulativeGasUsed: '0x0',
          gasUsed: '0x0',
          logs: null,
          contractAddress: null,
          from: '3065208b9e54d36617597bafb589993dfa2a6f483634c677912c086ec3b45659',
          to: '0x8af0d431acc6242c74164ac779894584dd110323',
          value: 'de0b6b3a7640000',
          data: '0x',
        },
        txId: '2231b1d718b4fcf82404feba2268230cb5600f2bcaaf48e3da930271fe00d28a',
        txFrom: '3065208b9e54d36617597bafb589993dfa2a6f483634c677912c086ec3b45659',
        hash: 'ca3b2cf426fdb0fea7bc888db53dad5cd694f9c08401707acd85675175c6c8b8',
      },
      txId: '2231b1d718b4fcf82404feba2268230cb5600f2bcaaf48e3da930271fe00d28a',
      timestamp: 1651139615197,
      hash: 'ca3b2cf426fdb0fea7bc888db53dad5cd694f9c08401707acd85675175c6c8b8',
      stateId: 'ca3b2cf426fdb0fea7bc888db53dad5cd694f9c08401707acd85675175c6c8b8',
    });
  }
  return result;
}
for (let i = 0; i < 1000; i++) {
  baseRecord.data.partitionHashes[i] = generateHash(64);

  const partitionReceiptMap = {};
  for (let i = 0; i < 6; i++) {
    // 6 txs per partition
    partitionReceiptMap[generateHash(8)] = [generateHash(8), generateHash(8)];
  }
  baseRecord.receipt.partitionHashes[i] = partitionReceiptMap;

  const partitionTxs = {};
  for (let i = 0; i < 6; i++) {
    // 6 txs per partition
    partitionTxs[generateHash(8)] = getWrappedEthAccount(3); // assume 3 accounts involved in each tx
  }
  baseRecord.receipt.partitionTxsMap[i] = partitionTxs;
}

fs.writeFileSync('sample_100tps_1000nodes.json', JSON.stringify(baseRecord));
