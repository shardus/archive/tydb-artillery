import filesize = require('filesize');
import {Knex, knex} from 'knex';
import {performance} from 'node:perf_hooks';
import {hrtime} from 'node:process';
import {delay, exitHandler, generateRandomHash, loadDummyData, prepareSqliteQuery} from './utils';
const osu = require('node-os-utils');
const shell = require('shelljs');

const dbPath = './db.sqlite';
const config: Knex.Config = {
  client: 'sqlite3', // or 'better-sqlite3'
  connection: {
    filename: dbPath,
  },
  useNullAsDefault: true, // this settings is just to suppress warning
};

const knexInstance = knex(config);
const NS_PER_SEC = 1e9;
const BULKSIZE = 200;
let SHOULD_RUN_LOOP = true;
let txc = 0;
let timeTakenToInsertAllRecord = BigInt(0);

const createTable = async (db: any, cb: Function) => {
  // clear old one
  await shell.rm('-rf', dbPath, {async: true});
  // Create a table
  try {
    await db.schema.createTable('archive', (table: any) => {
      table.string('key');
      table.jsonb('value');
    });
    cb(null, db);
  } catch (e: any) {
    cb(e, null);
  }
};

const t0 = Date.now();
createTable(knexInstance, async (err: any, db: any) => {
  if (err) {
    throw Error(err);
  }

  const data = await loadDummyData('./sample.json');
  console.log('"timeTakenToInsertAllRecords","txc","tps","tInsert"');
  while (SHOULD_RUN_LOOP) {
    const query = prepareSqliteQuery(BULKSIZE, data);

    const p0 = performance.now();
    db('archive')
      .insert(query)
      .then(() => {
        txc += BULKSIZE;

        //high precision insert performance timer in nanosecond
        // const pdiff = hrtime(p0); // returns [x, y] -> x is second, and y is nanosecond
        const pfinal = performance.now() - p0;
        timeTakenToInsertAllRecord += BigInt(Math.round(pfinal));
        // reference: https://nodejs.org/api/process.html#process_process_hrtime_time

        //every interval of tx record data, pipe it to stdout
        if (txc % (BULKSIZE * 1000) === 0) {
          const timelapsed = (Date.now() - t0) / 1000; // duration of the test running

          const timeTakenToInsertAllRecordSEC = Number(timeTakenToInsertAllRecord / BigInt(1000));

          const tps = (txc / timelapsed).toFixed(1);
          console.log(`"${Math.round(timeTakenToInsertAllRecordSEC)}s","${txc}","${tps}","${pfinal.toFixed(2)}ms"`);
        }
      });
    await delay(5);
  }
});

exitHandler(async () => {
  SHOULD_RUN_LOOP = false;

  const timelapsed = (Date.now() - t0) / 1000;
  const mem = filesize(process.memoryUsage().rss, {round: 0});
  const cpu = await osu.cpu.usage();
  const size = shell.exec('du -sh db.sqlite', {silent: true}).stdout.replace('db.sqlite', '\r').trim();
  console.log('Final hardware profiles..');
  console.log(`timelapsed:${timelapsed}s, mem:${mem}, cpu:${cpu}%, dbsize:${size}`);

  console.log('Shutting down database...');
  // eslint-disable-next-line no-process-exit
  process.exit();
});
